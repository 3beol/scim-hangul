/** @file scim_hangul_imengine_setup.cpp
 * implementation of Setup Module of hangul imengine module.
 */

/*
 * Smart Common Input Method
 * 
 * Copyright (C) 2004-2006 Choe Hwanjin
 * Copyright (c) 2004-2006 James Su <suzhe@turbolinux.com.cn>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
 *
 * $Id: scim_hangul_imengine_setup.cpp,v 1.8 2006/10/23 12:42:47 hwanjin Exp $
 *
 */

#define Uses_SCIM_CONFIG_BASE
#define Uses_C_STRING

#include <gtk/gtk.h>

#ifdef HAVE_CONFIG_H
  #include <config.h>
#endif

#include <cstring>
#include <scim.h>
#include <gtk/scimkeyselection.h>

#ifdef HAVE_GETTEXT
  #include <libintl.h>
  #define _(String) dgettext(GETTEXT_PACKAGE,String)
  #define N_(String) (String)
#else
  #define _(String) (String)
  #define N_(String) (String)
  #define bindtextdomain(Package,Directory)
  #define textdomain(domain)
  #define bind_textdomain_codeset(domain,codeset)
#endif

#include <hangul.h>

using namespace scim;

#define scim_module_init hangul_imengine_setup_LTX_scim_module_init
#define scim_module_exit hangul_imengine_setup_LTX_scim_module_exit

#define scim_setup_module_create_ui       hangul_imengine_setup_LTX_scim_setup_module_create_ui
#define scim_setup_module_get_category    hangul_imengine_setup_LTX_scim_setup_module_get_category
#define scim_setup_module_get_name        hangul_imengine_setup_LTX_scim_setup_module_get_name
#define scim_setup_module_get_description hangul_imengine_setup_LTX_scim_setup_module_get_description
#define scim_setup_module_load_config     hangul_imengine_setup_LTX_scim_setup_module_load_config
#define scim_setup_module_save_config     hangul_imengine_setup_LTX_scim_setup_module_save_config
#define scim_setup_module_query_changed   hangul_imengine_setup_LTX_scim_setup_module_query_changed


#define SCIM_CONFIG_IMENGINE_HANGUL_USE_DVORAK                  "/IMEngine/Hangul/UseDvorak"
#define SCIM_CONFIG_IMENGINE_HANGUL_SHOW_CANDIDATE_COMMENT      "/IMEngine/Hangul/ShowCandidateComment"
#define SCIM_CONFIG_IMENGINE_HANGUL_HANGUL_HANJA_KEY            "/IMEngine/Hangul/HangulHanjaKey"

#define SCIM_CONFIG_SHOW_CANDIDATE_COMMENT "/IMEngine/Hangul/ShowCandidateComment"
/*
#define SCIM_CONFIG_LAYOUT                 "/IMEngine/Hangul/KeyboardLayout"
*/
#define SCIM_CONFIG_LAYOUT_HANGUL          "/IMEngine/Hangul/KeyboardLayoutHangul"
#define SCIM_CONFIG_HANGUL_KEY             "/IMEngine/Hangul/HangulKey"
#define SCIM_CONFIG_HANJA_KEY              "/IMEngine/Hangul/HanjaKey"
#define SCIM_CONFIG_HANJA_MODE_KEY         "/IMEngine/Hangul/HanjaModeKey"
#define SCIM_CONFIG_USE_ASCII_MODE         "/IMEngine/Hangul/UseAsciiMode"
#define SCIM_CONFIG_COMMIT_BY_WORD         "/IMEngine/Hangul/CommitByWord"
#define SCIM_CONFIG_AUTO_REORDER           "/IMEngine/Hangul/AutoReorder"


#define VIEW_WIDTH 320
#define VIEW_HEIGHT 240



static GtkWidget * create_setup_window ();
static void        load_config (const ConfigPointer &config);
static void        save_config (const ConfigPointer &config);
static bool        query_changed ();

// Module Interface.
extern "C" {
    void scim_module_init (void)
    {
        bindtextdomain (GETTEXT_PACKAGE, SCIM_HANGUL_LOCALEDIR);
        bind_textdomain_codeset(GETTEXT_PACKAGE, "UTF-8");
    }

    void scim_module_exit (void)
    {
    }

    GtkWidget * scim_setup_module_create_ui (void)
    {
        static GtkWidget *setup_ui = NULL;
        if (setup_ui == NULL)
            setup_ui = create_setup_window ();
        return setup_ui;
    }

    String scim_setup_module_get_category (void)
    {
        return String ("IMEngine");
    }

    String scim_setup_module_get_name (void)
    {
        return String (_("Hangul"));
    }

    String scim_setup_module_get_description (void)
    {
        return String (_("A Hangul IMEngine Module."));
    }

    void scim_setup_module_load_config (const ConfigPointer &config)
    {
        load_config (config);
    }

    void scim_setup_module_save_config (const ConfigPointer &config)
    {
        save_config (config);
    }

    bool scim_setup_module_query_changed ()
    {
        return query_changed ();
    }
} // extern "C"

// Internal data structure
struct KeyBindingData
{
    const char *key;
    const char *label;
    const char *title;
    const char *tooltip;
    GtkWidget  *entry;
    GtkWidget  *button;
    String      data;
};


static GtkWidget *keyboard_select_dialog = NULL;

static GtkWidget *use_ascii_mode_button = NULL;
static GtkWidget *commit_by_word_button = NULL;
static GtkWidget *show_candidate_comment_button = NULL;
static GtkWidget *auto_reorder_button = NULL;

static bool __have_changed                 = false;

static KeyBindingData keyboard_bindings[] =
{
    {
        // key
        SCIM_CONFIG_LAYOUT_HANGUL,
        // label
        N_("Hangul Keyboard:"),
        // title
        N_("Select a Hangul Keyboard"),
        // tooltip
        N_("2"),
        // entry
        NULL,
        // button
        NULL,
        // data
        "Dubeolsik"
    }
};
static KeyBindingData key_bindings[] =
{
    {
        // key
        SCIM_CONFIG_HANGUL_KEY,
        // label
        N_("Hangul keys:"),
        // title
        N_("Select Hangul keys"),
        // tooltip
        N_("The key events to change input mode between hangul and ascii."
           "Click on the button on the right to edit it."),
        // entry
        NULL,
        // button
        NULL,
        // data
        "Hangul,"
        "Shift+space"
    },
    {
        // key
        SCIM_CONFIG_HANJA_KEY,
        // label
        N_("Hangul to Hanja keys:"),
        // title
        N_("Select Hangul to Hanja keys"),
        // tooltip
        N_("The key events to convert Hangul to Hanja character. "
           "Click on the button on the right to edit it."),
        // entry
        NULL,
        // button
        NULL,
        // data
        "Hangul_Hanja,"
        "F9"
    },
    {
        // key
        SCIM_CONFIG_HANJA_MODE_KEY,
        // label
        N_("Hanja Lock keys:"),
        // title
        N_("Select a key to toggle hanja mode keys"),
        // tooltip
        N_("The key events to toggle Hanja mode. "
           "Click on the button on the right to edit it."),
        // entry
        NULL,
        // button
        NULL,
        // data
        ""
    }
};

// Declaration of internal functions.
static void
on_default_editable_changed          (GtkEditable     *editable,
                                      gpointer         user_data);

static void
on_default_toggle_button_toggled     (GtkToggleButton *togglebutton,
                                      gpointer         user_data);


gboolean 
on_dialog_response (GtkDialog *dialog,
                    gint       response_id,
                    gpointer   user_data);

GtkWidget *
build_content_area                  (KeyBindingData *keyboard, 
                                    bool showing_extra);

static void
on_default_keyboard_selection_clicked (GtkButton *button,
                                        gpointer   user_data);
static void
on_default_key_selection_clicked     (GtkButton       *button,
                                      gpointer         user_data);

static void
on_default_combo_box_changed         (GtkComboBox *combobox,
                                      gpointer     user_data);

#if GTK_CHECK_VERSION(3, 0, 0)
#define gtk_hbox_new(a, b)  gtk_box_new(GTK_ORIENTATION_HORIZONTAL, b)
#define gtk_vbox_new(a, b)  gtk_box_new(GTK_ORIENTATION_VERTICAL, b)
#endif

#if GTK_CHECK_VERSION(2, 12, 0)
static GtkWidget *
create_options_page();

static GtkWidget *
create_keyboard_page();
#else
static GtkWidget *
create_options_page(GtkTooltips *tooltip);

static GtkWidget *
create_keyboard_page(GtkTooltips *tooltip);
#endif

// Function implementations.
#if GTK_CHECK_VERSION(2, 12, 0)
static GtkWidget *
create_options_page()
#else
static GtkWidget *
create_options_page(GtkTooltips *tooltips)
#endif
{
    GtkWidget *vbox;
    GtkWidget *button;

    vbox = gtk_vbox_new (FALSE, 12);
    gtk_container_set_border_width(GTK_CONTAINER(vbox), 12);

    button = gtk_check_button_new_with_mnemonic (_("_Use ascii input mode"));
    gtk_box_pack_start(GTK_BOX(vbox), button, FALSE, FALSE, 0);
#if GTK_CHECK_VERSION(2, 12, 0)
    gtk_widget_set_tooltip_text(button,
                          _("Whether to enable to change the input mode "
                            "between hangul and ascii mode."));
#else
    gtk_tooltips_set_tip(tooltips, button,
                          _("Whether to enable to change the input mode "
                            "between hangul and ascii mode."), NULL);
#endif
    g_signal_connect(G_OBJECT(button), "toggled",
                     G_CALLBACK(on_default_toggle_button_toggled), NULL);
    use_ascii_mode_button = button;

    button = gtk_check_button_new_with_mnemonic (_("_Show candidate comment"));
    gtk_box_pack_start(GTK_BOX(vbox), button, FALSE, FALSE, 0);
#if GTK_CHECK_VERSION(2, 12, 0)
    gtk_widget_set_tooltip_text(button,
            _("Whether to show the comment of candidates or not."));
#else
    gtk_tooltips_set_tip(tooltips, button,
                          _("Whether to show the comment of candidates or not."), NULL);
#endif
    g_signal_connect(G_OBJECT(button), "toggled",
                     G_CALLBACK(on_default_toggle_button_toggled), NULL);
    show_candidate_comment_button = button;

    button = gtk_check_button_new_with_mnemonic (_("_Commit by word"));
    gtk_box_pack_start(GTK_BOX(vbox), button, FALSE, FALSE, 0);
#if GTK_CHECK_VERSION(2, 12, 0)
    gtk_widget_set_tooltip_text(button,
            _("Whether not to commit until any non-hangul character is inputed."));
#else
    gtk_tooltips_set_tip(tooltips, button,
                          _("Whether not to commit until any non-hangul character is inputed."), NULL);
#endif
    g_signal_connect(G_OBJECT(button), "toggled",
                     G_CALLBACK(on_default_toggle_button_toggled), NULL);
    commit_by_word_button = button;

    /* Automatic reorder */
    button = gtk_check_button_new_with_mnemonic (_("Automatic _reordering"));
    gtk_box_pack_start(GTK_BOX(vbox), button, FALSE, FALSE, 0);
#if GTK_CHECK_VERSION(2, 12, 0)
    gtk_widget_set_tooltip_text(button,
            _("Whether to reorder jamo automatically, if they are in wrong order."));
#else
    gtk_tooltips_set_tip(tooltips, button,
            _("Whether to reorder jamo automatically, if they are in wrong order."));
#endif
    g_signal_connect(G_OBJECT(button), "toggled",
                     G_CALLBACK(on_default_toggle_button_toggled), NULL);
    auto_reorder_button = button;

    return vbox;
}

#if GTK_CHECK_VERSION(2, 12, 0)
static GtkWidget *
create_keyboard_page()
#else
static GtkWidget *
create_keyboard_page(GtkTooltips *tooltips)
#endif
{
    unsigned int i;

    GtkWidget *vbox1 = gtk_vbox_new(FALSE, 12);
    gtk_container_set_border_width(GTK_CONTAINER(vbox1), 12);

    GtkWidget *label = gtk_label_new("");
    gtk_label_set_use_markup(GTK_LABEL(label), TRUE);
    gtk_label_set_yalign (GTK_LABEL(label), 0.5);
    gtk_label_set_xalign (GTK_LABEL(label), 0.0);
    gtk_label_set_markup(GTK_LABEL(label),
            _("<span weight=\"bold\">Keyboard layout</span>"));
    gtk_box_pack_start(GTK_BOX(vbox1), label, FALSE, TRUE, 0);
//**** combo
/*
    GtkWidget *hbox = gtk_hbox_new(FALSE, 0);
    gtk_box_pack_start(GTK_BOX(vbox1), hbox, FALSE, TRUE, 0);

    label = gtk_label_new("    ");
    gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, TRUE, 0);

    GtkWidget *vbox2 = gtk_vbox_new(FALSE, 0);
    gtk_box_pack_start(GTK_BOX(hbox), vbox2, FALSE, TRUE, 0);

#if GTK_CHECK_VERSION(2, 24, 0)
    GtkWidget *combo_box = gtk_combo_box_text_new();
#else
    GtkWidget *combo_box = gtk_combo_box_new_text();
#endif
    gtk_box_pack_start(GTK_BOX(vbox2), combo_box, FALSE, TRUE, 0);

    unsigned int n = hangul_ic_get_n_keyboards();
    for (i = 0; i < n; i++) {
	const char* name = hangul_ic_get_keyboard_name(i);
#if GTK_CHECK_VERSION(2, 24, 0)
	gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(combo_box), NULL, name);
#else
	gtk_combo_box_append_text(GTK_COMBO_BOX(combo_box), name);
#endif
    }
    g_signal_connect(G_OBJECT(combo_box), "changed",
                     G_CALLBACK (on_default_combo_box_changed), NULL);
    keyboard_layout_combo = combo_box;
*/
//*****************
//***  3beol
    for (i = 0; i < G_N_ELEMENTS(keyboard_bindings); ++ i) {
        GtkWidget *hbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
        gtk_box_pack_start(GTK_BOX(vbox1), hbox, FALSE, TRUE, 0);

        label = gtk_label_new (NULL);
        gtk_label_set_text_with_mnemonic (GTK_LABEL (label), _(keyboard_bindings[i].label));
        gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, TRUE, 4);

        keyboard_bindings[i].button = gtk_button_new_with_label ("...");
        gtk_box_pack_end(GTK_BOX(hbox), keyboard_bindings[i].button, FALSE, TRUE, 4);
        gtk_label_set_mnemonic_widget (GTK_LABEL (label), keyboard_bindings[i].button);

        keyboard_bindings[i].entry = gtk_entry_new ();
        gtk_box_pack_end(GTK_BOX(hbox), keyboard_bindings[i].entry, FALSE, TRUE, 4);
        gtk_editable_set_editable (GTK_EDITABLE (keyboard_bindings[i].entry), FALSE);
        gtk_entry_set_text (GTK_ENTRY (keyboard_bindings[i].entry),
                            keyboard_bindings[i].tooltip);

        g_signal_connect(G_OBJECT(keyboard_bindings[i].button), "clicked",
                         G_CALLBACK (on_default_keyboard_selection_clicked),
                         &keyboard_bindings[i]);
        g_signal_connect(G_OBJECT(keyboard_bindings[i].entry), "changed",
                         G_CALLBACK (on_default_editable_changed), NULL);

        gtk_widget_set_tooltip_text(GTK_WIDGET(keyboard_bindings[i].entry),
                              keyboard_bindings[i].data.c_str());
    }
//**** 3beol

    label = gtk_label_new("");
    gtk_label_set_use_markup(GTK_LABEL(label), TRUE);
    gtk_label_set_xalign (GTK_LABEL(label), 0.0);
    gtk_label_set_yalign (GTK_LABEL(label), 0.5);
    gtk_label_set_markup(GTK_LABEL(label),
            _("<span weight=\"bold\">Key bindings</span>"));
    gtk_box_pack_start(GTK_BOX(vbox1), label, FALSE, TRUE, 0);

    for (i = 0; i < G_N_ELEMENTS(key_bindings); ++ i) {
        GtkWidget *hbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
        gtk_box_pack_start(GTK_BOX(vbox1), hbox, FALSE, TRUE, 0);

        label = gtk_label_new (NULL);
        gtk_label_set_text_with_mnemonic (GTK_LABEL (label), _(key_bindings[i].label));
        gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, TRUE, 4);

        key_bindings[i].button = gtk_button_new_with_label ("...");
        gtk_label_set_mnemonic_widget (GTK_LABEL (label), key_bindings[i].button);
        gtk_box_pack_end(GTK_BOX(hbox), key_bindings[i].button, FALSE, TRUE, 4);

        key_bindings[i].entry = gtk_entry_new ();
        gtk_editable_set_editable (GTK_EDITABLE (key_bindings[i].entry), FALSE);
        gtk_entry_set_text (GTK_ENTRY (key_bindings[i].entry),
                            key_bindings[i].data.c_str());
        gtk_box_pack_end(GTK_BOX(hbox), key_bindings[i].entry, FALSE, TRUE, 4);

        g_signal_connect(G_OBJECT(key_bindings[i].button), "clicked",
                         G_CALLBACK (on_default_key_selection_clicked),
                         &key_bindings[i]);
        g_signal_connect(G_OBJECT(key_bindings[i].entry), "changed",
                         G_CALLBACK (on_default_editable_changed), NULL);

#if GTK_CHECK_VERSION(2, 12, 0)
        gtk_widget_set_tooltip_text(key_bindings[i].entry,
                              _(key_bindings[i].tooltip));
#else
        gtk_tooltips_set_tip(tooltips, key_bindings[i].entry,
                              _(key_bindings[i].tooltip), NULL);
#endif
    }

    return vbox1;
}

static GtkWidget *
create_setup_window ()
{
    GtkWidget *notebook;
    GtkWidget *label;
    GtkWidget *page;

#if GTK_CHECK_VERSION(2, 12, 0)
#else
    GtkTooltips *tooltips = gtk_tooltips_new ();
#endif

    // Create the Notebook.
    notebook = gtk_notebook_new ();

    // Create the first page.
#if GTK_CHECK_VERSION(2, 12, 0)
    page = create_keyboard_page();
#else
    page = create_keyboard_page(tooltips);
#endif
    label = gtk_label_new (_("Keyboard"));
    gtk_notebook_append_page (GTK_NOTEBOOK (notebook), page, label);

    // Create the second page.
#if GTK_CHECK_VERSION(2, 12, 0)
    page = create_options_page();
#else
    page = create_options_page(tooltips);
#endif
    label = gtk_label_new (_("Options"));
    gtk_notebook_append_page (GTK_NOTEBOOK (notebook), page, label);

    gtk_notebook_set_current_page(GTK_NOTEBOOK (notebook), 0);

    gtk_widget_show_all(notebook);

    return notebook;
}

static void
load_config (const ConfigPointer &config)
{
    if (config.null())
        return;

    // keyboard layout option
    /*
    String layout = config->read(String(SCIM_CONFIG_LAYOUT), String("2"));
    int n = hangul_ic_get_n_keyboards();
    for (int i = 0; i < n; ++i) {
	const char* id = hangul_ic_get_keyboard_id(i);
	if (layout == id) {
	    gtk_combo_box_set_active(GTK_COMBO_BOX(keyboard_layout_combo), i);
	    break;
	}
    }
    */
    // keyboard bindings
    for (unsigned int i = 0; i < G_N_ELEMENTS(keyboard_bindings); ++ i) {
        String layout = 
            config->read (String (keyboard_bindings[i].key), keyboard_bindings[i].data);

        int n = hangul_keyboard_list_get_count();
        for (int index = 0; index < n; ++index) {
            const char* id = hangul_keyboard_list_get_keyboard_id(index);
            const char* name = hangul_keyboard_list_get_keyboard_name(index);
            if (g_strcmp0 (id, layout.c_str()) == 0) {
                gtk_entry_set_text(GTK_ENTRY(keyboard_bindings[i].entry), name);
                gtk_widget_set_tooltip_text(GTK_WIDGET(keyboard_bindings[i].entry), id);
                break;
            }
        }
    }

    // key bindings
    for (unsigned int i = 0; i < G_N_ELEMENTS(key_bindings); ++ i) {
        String text = 
            config->read (String (key_bindings[i].key), key_bindings[i].data);
        gtk_entry_set_text(GTK_ENTRY(key_bindings[i].entry), text.c_str());
    }

    bool stat;

    // use ascii input mode
    stat = config->read(String(SCIM_CONFIG_USE_ASCII_MODE), false);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(use_ascii_mode_button), stat);

    // show candidate comment option
    stat = config->read(String(SCIM_CONFIG_SHOW_CANDIDATE_COMMENT), true);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(show_candidate_comment_button), stat);

    // commit by word or not
    stat = config->read(String(SCIM_CONFIG_COMMIT_BY_WORD), false);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(commit_by_word_button), stat);

    // Automatic reordering mode
    stat = config->read(String(SCIM_CONFIG_AUTO_REORDER), true);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(auto_reorder_button), stat);

    __have_changed = false;
}

void
save_config (const ConfigPointer &config)
{
    if (config.null())
        return;

    /*
    int no = gtk_combo_box_get_active(GTK_COMBO_BOX(keyboard_layout_combo));
    const char* id = hangul_ic_get_keyboard_id(no);
    String layout = id == NULL ? "2" : id;
    config->write(String(SCIM_CONFIG_LAYOUT), layout);
    */
    for (unsigned int i = 0; i < G_N_ELEMENTS(keyboard_bindings); ++ i) {
        String text = gtk_widget_get_tooltip_text(GTK_WIDGET(keyboard_bindings[i].entry));
        config->write(String (keyboard_bindings[i].key), text);
    }

    for (unsigned int i = 0; i < G_N_ELEMENTS(key_bindings); ++ i) {
        String text = gtk_entry_get_text(GTK_ENTRY(key_bindings[i].entry));
        config->write(String (key_bindings[i].key), text);
    }

    gboolean stat;

    // use ascii input mode
    stat = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(use_ascii_mode_button));
    config->write(String(SCIM_CONFIG_USE_ASCII_MODE), (bool)stat);

    // show candidate comment
    stat = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(show_candidate_comment_button));
    config->write(String(SCIM_CONFIG_SHOW_CANDIDATE_COMMENT), (bool)stat);

    // commit by word or not
    stat = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(commit_by_word_button));
    config->write(String(SCIM_CONFIG_COMMIT_BY_WORD), (bool)stat);

    // Automatic reordering mode
    stat = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(auto_reorder_button));
    config->write(String(SCIM_CONFIG_AUTO_REORDER), (bool)stat);

    __have_changed = false;
}

bool
query_changed ()
{
    return __have_changed;
}

static void
on_default_editable_changed (GtkEditable *editable,
                             gpointer     user_data)
{
    __have_changed = true;
}

static void
on_default_toggle_button_toggled (GtkToggleButton *togglebutton,
                                  gpointer         user_data)
{
    __have_changed = true;
}

static void
on_default_combo_box_changed(GtkComboBox *combobox,
                             gpointer     user_data)
{
    __have_changed = true;
}

static void
on_default_keyboard_selection_clicked (GtkButton *button,
                                  gpointer   user_data)
{
    KeyBindingData *data = static_cast<KeyBindingData *> (user_data);

    if (data) {
        gboolean rebuild = false;
        gint result;

        keyboard_select_dialog = build_content_area (data, false);
    }
    
}

const gchar*
scim_keyboard_selection_get_id_name (GtkWidget *dialog, bool select = false)
{
    g_return_val_if_fail (GTK_DIALOG (dialog), NULL);

    GtkWidget *content_area = gtk_dialog_get_content_area (GTK_DIALOG (dialog));
    //g_print("GTK_IS_CONTAINER(content_area) : %s\n", GTK_IS_CONTAINER(content_area)? "true":"false");
    if(GTK_IS_CONTAINER(content_area)) {
        GList *children = gtk_container_get_children(GTK_CONTAINER(content_area));
        while (children != NULL) {
            // GtkScrolledWindow
            //g_print("(children->data) name : %s\n", gtk_widget_get_name(GTK_WIDGET(children->data)));
            if (GTK_IS_BIN(children->data)) {// GtkScrolledWindow
                //GtkViewport
                GtkWidget *child = gtk_bin_get_child(GTK_BIN(children->data));
                //g_print("(child) name : %s\n", gtk_widget_get_name(GTK_WIDGET(child)));
                if (GTK_IS_BIN(child)) {// GtkViewport
                    // GtkListBox
                    GtkWidget *child2 = gtk_bin_get_child(GTK_BIN(child));
                    //g_print("(child2) name : %s\n", gtk_widget_get_name(GTK_WIDGET(child2)));
                    if (GTK_IS_LIST_BOX(child2)){// GtkListBox
                        GtkListBoxRow *row = gtk_list_box_get_selected_row (GTK_LIST_BOX(child2));
                        GtkWidget *selected = gtk_bin_get_child (GTK_BIN (row));
                        if (g_strcmp0 (gtk_widget_get_name (GTK_WIDGET (selected)), "more") == 0) {
                            return NULL;
                        } else {
                            if (select) {// name
                                return gtk_label_get_text (GTK_LABEL(selected));
                            } else {// id
                                return gtk_widget_get_tooltip_text (GTK_WIDGET(selected));
                            }
                        }
                    }
                }
                break;// GtkScrolledWindow
            }

            children = g_list_next(children);
        }
    }

    return NULL;
}

GtkWidget *
more_row_new (void)
{
  GtkWidget *row = gtk_box_new (GTK_ORIENTATION_HORIZONTAL, 5);

  gtk_widget_set_tooltip_markup ( GTK_WIDGET (row), _("More..."));
  gtk_widget_set_name (GTK_WIDGET (row), "more");

  GtkWidget *arrow = gtk_image_new_from_icon_name ("view-more-symbolic", GTK_ICON_SIZE_MENU);
  gtk_widget_set_margin_start (GTK_WIDGET (arrow), 20);
  gtk_widget_set_margin_end (GTK_WIDGET (arrow), 20);
  gtk_widget_set_margin_top (GTK_WIDGET (arrow), 6);
  gtk_widget_set_margin_bottom (GTK_WIDGET (arrow), 6);
  gtk_widget_set_halign (GTK_WIDGET (arrow), GTK_ALIGN_CENTER);
  gtk_widget_set_valign (GTK_WIDGET (arrow), GTK_ALIGN_CENTER);

  gtk_box_pack_start (GTK_BOX (row), GTK_WIDGET (arrow), TRUE, TRUE, 0);

  return row;
}

void
add_list_item (GtkListBox *listbox, const gchar *text, const gchar *tooltip, bool first)
{
    GtkWidget     *item;
    gchar *name = g_strdup(text);
    gchar *id = g_strdup(tooltip);

    item = gtk_label_new(name);
    gtk_widget_set_tooltip_markup(item, id);
    //gtk_container_add (GTK_CONTAINER (listbox), item);

    if (first == TRUE)
    {
      gtk_list_box_insert (GTK_LIST_BOX (listbox), GTK_WIDGET (item), 0);
    }
    else 
    {
      gtk_list_box_insert (GTK_LIST_BOX (listbox), GTK_WIDGET (item), -1);
    }

    gtk_widget_show (item);

    g_free(id);
    g_free(name);
}

gboolean 
listbox_activated_row (GtkListBox    *box,
               GtkListBoxRow *row,
               gpointer       user_data)
{
  GtkWidget *dialog;
  GtkWidget *child;

  KeyBindingData *keyboard = (KeyBindingData *) user_data;

  child = gtk_bin_get_child (GTK_BIN (row));
  if (g_strcmp0 (gtk_widget_get_name (GTK_WIDGET (child)), "more") == 0)
  {
    gtk_widget_destroy (keyboard_select_dialog);
    keyboard_select_dialog = build_content_area (keyboard, true);
    return TRUE;
  }

  return FALSE;
}

gboolean 
listbox_key_pressed_row (GtkListBox    *box,
               GdkEvent  *event,
               gpointer       user_data)
{
  GtkWidget *dialog;
  GtkListBoxRow *row;
  GtkWidget *child;
  GdkEventKey *event_key = (GdkEventKey *) event;

  if (event_key->type != GDK_KEY_PRESS)
  {
    return FALSE;
  }

  if (event_key->hardware_keycode != 36 && // enter
      event_key->hardware_keycode != 65)  // space
  {
    return FALSE;
  }

  KeyBindingData *keyboard = (KeyBindingData *) user_data;

  row = gtk_list_box_get_selected_row (GTK_LIST_BOX (box));
  child = gtk_bin_get_child (GTK_BIN (row));
  if (g_strcmp0 (gtk_widget_get_name (GTK_WIDGET (child)), "more") == 0)
  {
    return FALSE;
  }

  on_dialog_response (GTK_DIALOG (keyboard_select_dialog), GTK_RESPONSE_OK, keyboard);

  return TRUE;
}


void
layout_view_clicked(KeyBindingData *keyboard)
{
  gchar *extension_image = g_strdup(".png");
  gchar *extension_text = g_strdup(".txt");
  gchar *layout = NULL;

  gchar *filename_layout = NULL;
  gchar *filename_description = NULL;
  
  const gchar *name = scim_keyboard_selection_get_id_name (GTK_WIDGET (keyboard_select_dialog), true);
  const gchar *id = scim_keyboard_selection_get_id_name (GTK_WIDGET (keyboard_select_dialog), false);

  if (g_str_has_prefix(id, "2"))
  {
    layout = g_strdup("2set_");
  }
  else if (g_str_has_prefix(id, "3") |
            g_str_has_prefix(id, "ahn"))
  {
    layout = g_strdup("3set_");
  }
  else if (g_str_has_prefix(id, "ro"))
  {
    layout = g_strdup("kb_us_");
  }
  else
  {
    return;
  }


  gchar *help_layout = NULL;
  gchar *help_txt = NULL;

  filename_layout = g_strjoin(NULL, layout, id, extension_image, NULL);
  filename_description = g_strjoin(NULL, layout, id, extension_text, NULL);
  help_layout = g_strjoin("/", LIBHANGUL_DATA_DIR, "keyboards_info", filename_layout, NULL);
  help_txt = g_strjoin("/", LIBHANGUL_DATA_DIR, "keyboards_info", filename_description, NULL);

  GtkWidget *dialog_layout;
  GtkWidget *content_area;
  GtkDialogFlags flags;

  #if GTK_CHECK_VERSION (3, 12, 0)
  flags = (GtkDialogFlags) (GTK_DIALOG_MODAL | GTK_DIALOG_USE_HEADER_BAR);
#else
  flags = (GtkDialogFlags) GTK_DIALOG_MODAL;
#endif

  dialog_layout = gtk_dialog_new_with_buttons (
                        _(name),
                        GTK_WINDOW (NULL),
                        flags,
                        _("_OK"), GTK_RESPONSE_CANCEL,
                        NULL);
  g_signal_connect (dialog_layout, "destroy",
                    G_CALLBACK (gtk_widget_destroy), NULL);

  gtk_window_set_icon_name (GTK_WINDOW (dialog_layout), "scim-logo");
  gtk_widget_set_size_request (GTK_WIDGET (dialog_layout), -1, -1);
  gtk_window_set_resizable (GTK_WINDOW (dialog_layout), FALSE);

  content_area = gtk_dialog_get_content_area (GTK_DIALOG (dialog_layout));

  GtkWidget *label;
  GdkPixbuf *pixbuf;
  GError *error = NULL;

  if (g_file_test (help_layout, G_FILE_TEST_IS_REGULAR))
  {
    pixbuf = gdk_pixbuf_new_from_file_at_size (help_layout, 520, 420, &error);
    label = gtk_image_new_from_pixbuf (pixbuf);
    gtk_box_pack_start (GTK_BOX (content_area), label, TRUE, TRUE, 0);
  }
  else
  {
    label = gtk_label_new(help_layout);
    gtk_box_pack_start (GTK_BOX (content_area), label, TRUE, TRUE, 0);
  }

  GtkWidget *scrolled_window;
  GtkWidget *text_view;
  GtkTextTagTable *tab_table;
  GtkTextBuffer *buffer;
  GtkTextIter iter;
  GString *lines = g_string_new (NULL);

  if (g_file_test (help_txt, G_FILE_TEST_IS_REGULAR))
  {
    char line[1000];

    tab_table = gtk_text_tag_table_new ();
    buffer = gtk_text_buffer_new (tab_table);
    FILE *file;

    if ((file = fopen(help_txt, "r")) != NULL)
    {
      while (!feof (file))
      {	
        if (fgets (line, sizeof(line), file) != NULL)
        {
          g_string_append (lines, (gchar *)line);
        }
        
      }
    }
    fclose(file);

    gtk_text_buffer_set_text (buffer, lines->str, lines->len);
    gtk_text_buffer_get_iter_at_line (buffer, &iter, 0);
    gtk_text_buffer_place_cursor (buffer, &iter);

    text_view = gtk_text_view_new_with_buffer (buffer);
    gtk_text_view_set_editable (GTK_TEXT_VIEW (text_view), FALSE);
    gtk_text_view_set_wrap_mode (GTK_TEXT_VIEW (text_view), GTK_WRAP_CHAR);

    scrolled_window = gtk_scrolled_window_new (NULL, NULL);
    gtk_scrolled_window_set_policy (
                  GTK_SCROLLED_WINDOW (scrolled_window),
                  GTK_POLICY_NEVER,
                  GTK_POLICY_ALWAYS);
    gtk_scrolled_window_set_shadow_type(
                  GTK_SCROLLED_WINDOW (scrolled_window), 
                  GTK_SHADOW_IN);
    gtk_scrolled_window_set_min_content_height (
                  GTK_SCROLLED_WINDOW (scrolled_window), 
                  VIEW_HEIGHT * 1.5);
    gtk_container_add (
                  GTK_CONTAINER (scrolled_window), 
                  GTK_WIDGET(text_view));
    gtk_container_set_border_width (GTK_CONTAINER (scrolled_window), 5);

    gtk_box_pack_start (GTK_BOX (content_area), scrolled_window, TRUE, TRUE, 0);
  }
  else
  {
    label = gtk_label_new(help_txt);
    gtk_box_pack_end (GTK_BOX (content_area), label, TRUE, TRUE, 0);
  }
  
  gtk_widget_show_all (dialog_layout);
  gtk_dialog_run (GTK_DIALOG (dialog_layout));

  gtk_widget_destroy (GTK_WIDGET (dialog_layout));

  g_free (extension_image);
  g_free (extension_text);

  g_free (layout);

  g_free (filename_layout);
  g_free (filename_description);

  g_free (help_layout);
  g_free (help_txt);

  g_string_free (lines, TRUE);

}

gboolean 
on_dialog_response (GtkDialog *dialog,
                    gint       response_id,
                    gpointer   user_data)
{
  KeyBindingData *keyboard = (KeyBindingData *) user_data;

  switch (response_id)
  {
    case GTK_RESPONSE_HELP:
      {
        layout_view_clicked(keyboard);
      }
      break;
    case GTK_RESPONSE_OK:
      {
        const gchar *id = scim_keyboard_selection_get_id_name (
                                    GTK_WIDGET (keyboard_select_dialog), false);

        if (!id) {
            id = "2";
        }

        if (strcmp (id, gtk_widget_get_tooltip_text (GTK_WIDGET (keyboard->entry))) != 0) {
            guint layoutCount = hangul_keyboard_list_get_count();
            guint index;
            for (index = 0; index < layoutCount; index++)
            {
                const gchar *id2 = hangul_keyboard_list_get_keyboard_id(index);
                const gchar *name = hangul_keyboard_list_get_keyboard_name(index);

                if (g_strcmp0 (id, id2) == 0) {
                    gtk_entry_set_text(GTK_ENTRY (keyboard->entry), name);
                    gtk_widget_set_tooltip_text(GTK_WIDGET (keyboard->entry), id2);
                }
            }
        }

        gtk_widget_destroy (GTK_WIDGET (keyboard_select_dialog));
        keyboard_select_dialog = NULL;
      }
      break;
    case GTK_RESPONSE_CANCEL:
    default:
      {
        gtk_widget_destroy (GTK_WIDGET (keyboard_select_dialog));
        keyboard_select_dialog = NULL;
      }
      break;
  }
  return TRUE;
}

GtkWidget *
build_content_area (KeyBindingData *keyboard, bool showing_extra)
{
  gchar *id1 = gtk_widget_get_tooltip_text(GTK_WIDGET (keyboard->entry));

  GtkWidget *dialog;
  GtkDialogFlags flags;
  GtkWidget *content_area;

  GtkWidget *scrolled_window;
  GtkListBox *list_box;
  GtkListBoxRow *list_box_row;


#if GTK_CHECK_VERSION (3, 12, 0)
  flags = (GtkDialogFlags) (GTK_DIALOG_MODAL | GTK_DIALOG_USE_HEADER_BAR);
#else
  flags = (GtkDialogFlags) GTK_DIALOG_MODAL;
#endif

  dialog = gtk_dialog_new_with_buttons (
                      _("Select a Keyboard Layout"),
                      GTK_WINDOW (NULL),
                      flags,
                      _("_Layout"), GTK_RESPONSE_HELP,
                      _("_OK"), GTK_RESPONSE_OK,
                      _("_Cancel"), GTK_RESPONSE_CANCEL,
                      NULL);

  //gtk_window_set_icon_name (GTK_WINDOW (dialog), "scim-logo");
  gtk_widget_set_size_request (GTK_WIDGET (dialog), 420, -1);
  gtk_window_set_resizable (GTK_WINDOW (dialog), FALSE);


  list_box = GTK_LIST_BOX (gtk_list_box_new ());
  gtk_list_box_set_selection_mode (GTK_LIST_BOX (list_box), GTK_SELECTION_BROWSE);
  
  guint layoutCount = hangul_keyboard_list_get_count();
  guint index;
  for (index = 0; index < layoutCount; index++)
  {
    const gchar *id2 = hangul_keyboard_list_get_keyboard_id(index);
    const gchar *name = hangul_keyboard_list_get_keyboard_name(index);

    if (g_strcmp0 (id1, id2) == 0) 
    {
      add_list_item (list_box, name, id2, TRUE);
      list_box_row = gtk_list_box_get_row_at_index(GTK_LIST_BOX(list_box), 0);
      gtk_list_box_select_row(GTK_LIST_BOX (list_box), list_box_row);
    }
    else
    {
      if (showing_extra == FALSE)
      {
        gchar **initial_keys = g_strdupv ((gchar **)libhangul_get_init_keyboard_ids ());
        int i;
        for(i = 0; i < libhangul_get_init_keyboard_ids_length (); i++)
        {
          gchar *key = (gchar *)initial_keys[i];
          if(g_strcmp0(key, id2) == 0)
          {
            add_list_item (list_box, name, id2, FALSE);
          }
        }
      }
      else 
      {
        add_list_item (list_box, name, id2, FALSE);
      }
    }
  }

  if (showing_extra == FALSE)
  {
    gtk_list_box_insert (GTK_LIST_BOX (list_box), GTK_WIDGET (more_row_new()), -1);
  }

  g_signal_connect (GTK_LIST_BOX (list_box), "row-activated",
          G_CALLBACK (listbox_activated_row), keyboard);
  g_signal_connect (GTK_LIST_BOX (list_box), "key-press-event",
          G_CALLBACK (listbox_key_pressed_row), keyboard);

  g_signal_connect (GTK_DIALOG (dialog), "response", 
                    G_CALLBACK (on_dialog_response), keyboard);

  scrolled_window = gtk_scrolled_window_new (NULL, NULL);
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolled_window),
                                  GTK_POLICY_NEVER,
                                  GTK_POLICY_ALWAYS);
  gtk_scrolled_window_set_shadow_type(GTK_SCROLLED_WINDOW (scrolled_window), GTK_SHADOW_IN);

  gtk_container_add (GTK_CONTAINER (scrolled_window), GTK_WIDGET(list_box));
  gtk_container_set_border_width (GTK_CONTAINER (scrolled_window), 10);

  gtk_scrolled_window_set_min_content_width (GTK_SCROLLED_WINDOW (scrolled_window), VIEW_WIDTH);
  gtk_scrolled_window_set_min_content_height (GTK_SCROLLED_WINDOW (scrolled_window), VIEW_HEIGHT);


  content_area = gtk_dialog_get_content_area (GTK_DIALOG (dialog));
  gtk_box_pack_start (GTK_BOX (content_area), scrolled_window, TRUE, TRUE, 0);

  gtk_widget_show_all (dialog);

  g_free (id1);

  return dialog;

}
static void
on_default_key_selection_clicked (GtkButton *button,
                                  gpointer   user_data)
{
    KeyBindingData *data = static_cast<KeyBindingData *> (user_data);

    if (data) {
        GtkWidget *dialog = scim_key_selection_dialog_new (_(data->title));
        gint result;

        scim_key_selection_dialog_set_keys (
            SCIM_KEY_SELECTION_DIALOG (dialog),
            gtk_entry_get_text (GTK_ENTRY (data->entry)));

        result = gtk_dialog_run (GTK_DIALOG (dialog));

        if (result == GTK_RESPONSE_OK) {
            const gchar *keys = scim_key_selection_dialog_get_keys (
                            SCIM_KEY_SELECTION_DIALOG (dialog));

            if (!keys) keys = "";

            if (strcmp (keys, gtk_entry_get_text (GTK_ENTRY (data->entry))) != 0)
                gtk_entry_set_text (GTK_ENTRY (data->entry), keys);
        }

        gtk_widget_destroy (dialog);
    }
}

// vi:ts=8:sts=4:sw=4:nowrap:expandtab:
